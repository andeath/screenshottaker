package main

import (
	"context"
	"log"
	"os"
	"os/exec"
	"time"

	"github.com/google/uuid"
)

type ScreenshotTaker struct {
	Path       string
	UserAgent  string
	Resolution string
	Timeout    int
}

var path = "screenshot"
var userAgent = "Mozilla/5.0 (iPhone; CPU iPhone OS 11_0 like Mac OS X) " +
	"AppleWebKit/604.1.38 (KHTML, like Gecko) " +
	"Version/11.0 Mobile/15A372 Safari/604.1"

// ScreenshotURL takes a screenshot of a URL
func ScreenshotURL(targetURL string, parameters *RequestParameters) (destination string, err error) {
	//
	//log.WithFields(log.Fields{"url": targetURL, "full-destination": destination}).
	//	Debug("Full path to screenshot save using Chrome")
	destination = path + "/" + uuid.NewSHA1(uuid.NameSpaceURL, []byte(targetURL)).String() + ".png"
	parameters.Width = 1600
	parameters.Height = 1200
	parameters.UserAgent = userAgent
	// Start with the basic headless arguments
	var chromeArguments = []string{
		"--headless", "--disable-gpu", "--hide-scrollbars",
		"--disable-crash-reporter",
		//"--user-agent=" + parameters.UserAgent,
		"--window-size=" + parameters.Resolution.String(), "--screenshot=" + destination,
	}

	if parameters.Mobile {
		chromeArguments = append(chromeArguments, "--use-mobile-user-agent")
	}
	// When we are running as root, chromiun will flag the 'cant
	// run as root' thing. Handle that case.
	if os.Geteuid() == 0 {

		//log.WithField("euid", os.Geteuid()).Debug("Running as root, adding --no-sandbox")
		chromeArguments = append(chromeArguments, "--no-sandbox")
	}

	// Finally add the url to screenshot
	chromeArguments = append(chromeArguments, targetURL)

	//log.WithFields(log.Fields{"arguments": chromeArguments}).Debug("Google Chrome arguments")

	// get a context to run the command in
	ctx, cancel := context.WithTimeout(context.Background(), time.Duration(parameters.Timeout)*time.Second)
	defer cancel()

	// Prepare the command to run...
	chrome := "/home/as/.local/share/pyppeteer/local-chromium/575458/chrome-linux/chrome"
	cmd := exec.CommandContext(ctx, chrome, chromeArguments...)

	//log.WithFields(log.Fields{"url": targetURL, "destination": destination}).Info("Taking screenshot")

	// ... and run it!
	//startTime := time.Now()
	if err := cmd.Start(); err != nil {
		log.Fatal(err)
	}

	// Wait for the screenshot to finish and handle the error that may occur.
	if err := cmd.Wait(); err != nil {

		// If if this error was as a result of a timeout
		if ctx.Err() == context.DeadlineExceeded {
			//log.WithFields(log.Fields{"url": targetURL, "destination": destination, "err": err}).
			//	Error("Timeout reached while waiting for screenshot to finish")
			return "", err
		}

		//log.WithFields(log.Fields{"url": targetURL, "destination": destination, "err": err}).
		//	Error("Screenshot failed")

		return "", err
	}

	//log.WithFields(log.Fields{
	//	"url": targetURL, "destination": destination, "duration": time.Since(startTime),
	//}).Info("Screenshot taken")
	return
}
