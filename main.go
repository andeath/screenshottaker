package main

import (
	"context"
	"fmt"
	"log"
	"net/http"
	"os"
	"os/signal"
	"time"
	// third-party
	"github.com/gin-gonic/gin"
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promhttp"
)

var (
	//storage = FileStorage{}
	//workerPool  chan *Worker

	cpuTemp = prometheus.NewGauge(
		prometheus.GaugeOpts{
			Name: "cpu_temperature_celsius",
			Help: "Current temperature CPU.",
		})
	hdFailures = prometheus.NewCounterVec(
		prometheus.CounterOpts{
			Name: "hd_error_total",
			Help: "Number of hard-disk errors",
		},
		[]string{"device"},
	)
)

func init() {
	prometheus.MustRegister(cpuTemp)
	prometheus.MustRegister(hdFailures)
}

type Resolution struct {
	Width  int `form:"width"	    json:"width"`
	Height int `form:"height"	    json:"height"`
}

func (r *Resolution) String() string {
	return fmt.Sprintf("%v,%v", r.Width, r.Height)
}

type RequestParameters struct {
	Url       string `form:"url" 	    json:"url" 		binding:"required"`
	UserAgent string `form:"user_agent" json:"user_agent,omitempty"`
	Mobile    bool   `form:"mobile"     json:"mobile"`
	Timeout   int    `form:"timeout"	json:"timeout"`
	Resolution
}

func takeScreenshot(context *gin.Context) {
	var screenshot string
	var params RequestParameters
	//var worker *Worker

	if err := context.Bind(&params); err != nil {
		context.JSON(http.StatusBadRequest, gin.H{"status": "validation error", "error": err})
		return
	}

	//screenshot, ok := storage.get(params.Url, &params)
	//if ok {
	//	context.File(screenshot)
	//}

	//worker = <- workerPool
	//screenshot, err := worker.takeScreenshot(params.Url, &params)
	screenshot, err := ScreenshotURL(params.Url, &params)
	if err != nil {
		log.Println(err)
		context.JSON(http.StatusBadRequest, gin.H{"status": "error", "error": err})

	}
	context.File(screenshot)

}

func setupRouter() *gin.Engine {
	// Disable Console Color
	// gin.DisableConsoleColor()
	r := gin.Default()

	// Ping test
	r.GET("/ping", func(c *gin.Context) {
		c.String(http.StatusOK, "pong")
	})
	r.GET("/metrics", func(c *gin.Context) {
		promhttp.Handler().ServeHTTP(c.Writer, c.Request)
	})

	screenShots := r.Group("/api/screenshot")

	screenShots.GET("/", takeScreenshot)
	screenShots.POST("/", takeScreenshot)
	screenShots.Use()

	return r
}

func main() {
	router := setupRouter()

	//workerPool = initWorkerPool(2)

	srv := &http.Server{
		Addr:    ":55888",
		Handler: router,
	}

	go func() {
		// service connections
		if err := srv.ListenAndServe(); err != nil && err != http.ErrServerClosed {
			log.Fatalf("listen: %s\n", err)
		}
	}()

	// Wait for interrupt signal to gracefully shutdown the server with
	// a timeout of 5 seconds.
	quit := make(chan os.Signal)
	signal.Notify(quit, os.Interrupt)
	<-quit
	log.Println("Shutdown Server ...")

	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	if err := srv.Shutdown(ctx); err != nil {
		log.Fatal("Server Shutdown:", err)
	}
	log.Println("Server exiting")
}
